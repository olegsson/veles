import utils
tf = utils.tf_init()

from tensorflow.keras import layers
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from pathlib import Path
from sklearn.model_selection import train_test_split

import plotutil as pu

# clss, files = zip(*[(1, fp) for fp in Path('/data/work/lidar_plitvice/plitvice_voxels').glob('*.npy')] \
#     + [(0, fp) for fp in Path('/data/work/lidar_plitvice/plitvice_voxels_negative').glob('*.npy')])
#
# vox = np.stack([
#     np.load(fp)
#     for fp in files
# ]).astype(np.float16)
# vox = vox.reshape(*vox.shape, 1)

vox, clss, files = utils.load_data()

test_size = .2

#%%
x_train, x_test, y_train, y_test = train_test_split(vox, np.array(clss), test_size=test_size)

#%%

x = layers.Input(x_train.shape[1:])
e = layers.Conv3D(32, 7, 4, activation='relu', padding='same')(x)
e = layers.Conv3D(32, 5, 2, activation='relu', padding='same')(e)
e = layers.Flatten()(e)

e = layers.Dense(512, activation='relu')(e)

d = layers.Reshape((8, 8, 8, 1))(e)
d = layers.Conv3DTranspose(32, 5, 2, padding='same', activation='relu')(d)
d = layers.Conv3DTranspose(32, 7, 4, padding='same', activation='relu')(d)
d = layers.MaxPool3D((2, 2, 2), strides=2)(d)
d = layers.Conv3D(1, 1, 1, activation='sigmoid', padding='same')(d)

model = tf.keras.Model(x, d)
model.summary()

#%%
model.compile('adam', 'binary_crossentropy')
#%%
try:
    model.fit(
        x_train, x_train,
        batch_size=1,
        epochs=100,
        shuffle=True,
        validation_data=[x_test, x_test],
        callbacks=[
            tf.keras.callbacks.ModelCheckpoint(
                './models/ae_best.h5',
                save_best_only=True,
            ),
            tf.keras.callbacks.TensorBoard(log_dir='./logs'),
        ]
    )
except (KeyboardInterrupt, OSError):
    pass
finally:
    model.save('./models/ae_last.h5')
