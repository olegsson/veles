import utils
tf = utils.tf_init(nogpu=True)

from pathlib import Path
from random import choice
import numpy as np
import xgboost as xgb
from sklearn import metrics
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

import plotutil as pu

#%%

x, y, files = utils.load_data()

print(
    y.size,
    y.sum(),
    y.size-y.sum(),
)

#%%

ae = tf.keras.models.load_model('models/ae_last.h5')
ae.summary()

#%%

encoder = tf.keras.Model(ae.layers[0].input, ae.layers[4].output)
encoder.summary()

#%%

tf.keras.utils.plot_model(
    encoder,
    to_file='prezentacija_rezultata/encoder.png',
    show_shapes=True,
    show_layer_names=True,
    rankdir='TB',
    expand_nested=False,
    dpi=96,
)

tf.keras.utils.plot_model(
    ae,
    to_file='prezentacija_rezultata/ae.png',
    show_shapes=True,
    show_layer_names=True,
    rankdir='TB',
    expand_nested=False,
    dpi=96,
)


#%%


bst = xgb.Booster()
# bst.load_model('models/bst.model')
bst.load_model('models/bst_ev.model')
# bst.load_model('models/bst_ev_cv.model')

# len(bst.get_dump())
#%%

from matplotlib import pyplot as plt
%matplotlib inline
fig, ax = plt.subplots(figsize=(50, 50))
xgb.plot_tree(bst, ax=ax, num_trees=0)
plt.savefig('prezentacija_rezultata/bst0.png', dpi=300, bbox_inches='tight')

#%%

def predict(vox):
    e = encoder.predict(vox)
    return bst.predict(xgb.DMatrix(e))[0] >= .5

[*samples] = zip(x, y, files)

e = encoder.predict(x)
p = bst.predict(xgb.DMatrix(e)) >= .5

print(
    'precision: %f'%metrics.precision_score(y, p),
    'recall:    %f'%metrics.recall_score(y, p),
    'f1:        %f'%metrics.f1_score(y, p),
    metrics.confusion_matrix(y, p),
    sep='\n',
)

fpgen = (s for s in np.array(samples)[p & ~y])
fngen = (s for s in np.array(samples)[~p & y])
tpgen = (s for s in np.array(samples)[p & y])
tngen = (s for s in np.array(samples)[~p & ~y])
#%%

vox, clss, f = choice(samples)
# vox, clss, f = next(fpgen)
# vox, clss, f = next(fngen)
# vox, clss, f = next(tpgen)
# vox, clss, f = next(tngen)
vox = vox.reshape(1, *vox.shape)
pvox = ae.predict(vox) >= .5
pred = predict(vox)
print(
    'target:    %s'%clss,
    'predicted: %s'%pred,
sep='\n')

#%%

# pu.viewlas(f)
pu.pcdplot(f, background='#fff')
pu.voxplot(vox, background='#fff', edgecolor='#fff')
# pu.voxplot(pvox, background='#fff', edgecolor='#fff')

#%%

praw = bst.predict(xgb.DMatrix(e))

def _t_metric(y_, p_, metric='precision'):
    def wrapped(t):
        pt = p_ >= t
        return metrics.__dict__[f'{metric}_score'](y_, pt)
    return wrapped

def _tpr(y_, p_):
    def wrapped(t):
        pt = p_ >= t
        tp = (pt & y_).sum()
        fn = (y_ & ~pt).sum()
        return tp / (tp + fn)
    return wrapped

def _fpr(y_, p_):
    def wrapped(t):
        pt = p_ >= t
        fp = (pt & ~y_).sum()
        tn = (~y_ & ~pt).sum()
        return fp / (fp + tn)
    return wrapped

def ROCplot(y_, p_, steps=100):
    _t = np.linspace(0, 1, num=steps)
    tpr = [*map(_tpr(y_, p_), _t)]
    fpr = [*map(_fpr(y_, p_), _t)]
    plt.plot(fpr, tpr)

def PRplot(y_, p_, steps=100): # needs work
    _t = np.linspace(.0, 1, num=steps)
    _pre = [*map(_t_metric(y_, p_, 'precision'), _t)]
    _rec = [*map(_t_metric(y_, p_, 'recall'), _t)]
    plt.plot(_rec, _pre)

ROCplot(y, praw)
PRplot(y, praw)

#%%

scaler = StandardScaler()

e_scaled = scaler.fit_transform(e)
pca = PCA(n_components=.99, svd_solver='auto', whiten=True)

pc = pca.fit_transform(e_scaled)
lda = LinearDiscriminantAnalysis()
lda.fit(pc, y)

lda_p = lda.predict(pc)

print(
    'precision: %f'%metrics.precision_score(y, lda_p),
    'recall:    %f'%metrics.recall_score(y, lda_p),
    'f1:        %f'%metrics.f1_score(y, lda_p),
    metrics.confusion_matrix(y, lda_p),
    sep='\n',
)

from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier

lr = LogisticRegression()
lr.fit(pc, y)

lr_p = lr.predict(pc)

print(
    'precision: %f'%metrics.precision_score(y, lr_p),
    'recall:    %f'%metrics.recall_score(y, lr_p),
    'f1:        %f'%metrics.f1_score(y, lr_p),
    metrics.confusion_matrix(y, lr_p),
    sep='\n',
)
