import utils
tf = utils.tf_init(gpu_memlimit=1024)

import numpy as np
import xgboost as xgb
from sklearn.model_selection import train_test_split
from sklearn import metrics

# from multiprocessing import Pool

from evolution import Population


ae = tf.keras.models.load_model('models/ae_last.h5')
encoder = tf.keras.Model(ae.layers[0].input, ae.layers[4].output)

x, y, files = utils.load_data()
encoded = encoder.predict(x)

#%%
x_train, x_test, y_train, y_test = train_test_split(encoded, y, test_size=.2)

dtrain = xgb.DMatrix(x_train, y_train)
dtest = xgb.DMatrix(x_test)

def genfunc():
    return [np.random.choice(np.arange(*r)) for r in [
        (2, 15),         # max_depth
        # (2, 9),          # min_child_weight
        # (.4, 1.2, .2),   # subsample
        # (.4, 1.2, .2),   # colsample
        # (2, 11),         # gamma
        (50, 1000, 50),          # n_estimators
        (.1, 1.1, .1), # eta
    ]]

def makeparams(genome):
    params = {
        'objective':'binary:logistic',
        # 'eval_metric': 'error',
        # 'tree_method': 'gpu_hist',
    }

    keys = [
        'max_depth',
        # 'min_child_weight',
        # 'subsample',
        # 'colsample',
        # 'gamma',
        'n_estimators',
        'eta',
    ]

    params.update(dict(zip(keys, genome)))
    # params['max_depth'] = 2**params['max_depth']
    # params['n_estimators'] = 2**params['n_estimators']
    return params

def cv(params, num_boost_round=100):
    cv_results = xgb.cv(
        params,
        dtrain,
        num_boost_round=num_boost_round,
        # seed=42,
        nfold=3,
        metrics={'error'},
        early_stopping_rounds=10,
        stratified=True,
    )
    return cv_results

def fitness_cv(genome):
    results = cv(makeparams(genome))
    return results['test-error-mean'].min()

def fitness(genome):
    bst = xgb.train(
        makeparams(genome),
        dtrain,
        num_boost_round=10,
    )
    pred = bst.predict(dtest) >= .5
    return metrics.f1_score(y_test, pred)

pop = Population(
    genfunc,
    fitness_cv,
    # fitness,
    size=100,
    # fitness_max=False,
)
# pop.individuals
#%%
for i in range(10):
    print(pop.evolve(
        keep_best=.15,
        keep_losers=.15,
        make_offspring=.2,
    ))
# len(pop.individuals)
# pop.individuals
# pop.evaluate()
# pop.hall_of_fame
# pop.individuals
#%%

# params = {
#     'max_depth':2,
#     'eta':1,
#     'objective':'binary:logistic',
# }

params = makeparams(pop.hall_of_fame[0][0])

bst = xgb.train(
    params,
    dtrain,
    num_boost_round=10,
)
pred = bst.predict(dtest) >= .5

#%%

print(
    'precision: %f'%metrics.precision_score(y_test, pred),
    'recall:    %f'%metrics.recall_score(y_test, pred),
    'f1:        %f'%metrics.f1_score(y_test, pred),
    metrics.confusion_matrix(y_test, pred),
    sep='\n',
)

#%%
# bst.save_model('models/bst_ev.model')
# bst.save_model('models/bst_ev_cv.model')
