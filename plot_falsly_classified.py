import open3d as o3d
from pyntcloud import PyntCloud
import numpy as np
from pathlib import Path
import pandas as pd
import os
import json
from subprocess import run

import utils
import plotutil as pu

#%%

x, y, files = utils.load_data()

def plot_sample(i, parent_dir):
    if not Path(parent_dir).exists():
        os.makedirs(parent_dir)
    pu.voxplot(x[i], background='#fff', filename=f'{parent_dir}/{i}.png', block=True)

false = json.load(open('results/results_false.json'))

for ftype, vals in false.items():
    for i_fold, idxlist in enumerate(vals):
        if idxlist:
            print(ftype, i_fold+1, len(idxlist))
            for i in idxlist:
                plot_sample(i, f'tmp_voxplots/{ftype}/{i_fold+1}')
            run(
                f'montage tmp_voxplots/{ftype}/{i_fold+1}/* ' \
                '-geometry +0+0 -resize 500x500^ -gravity center -crop 500x500+0+0 ' \
                f'results/{ftype}_split{i_fold+1}.png',
                shell=True,
            )
#%%
