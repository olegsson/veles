import plotly
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
init_notebook_mode(connected=True)
import plotly.graph_objs as go
import numpy as np
from scipy.ndimage import zoom, convolve
from subprocess import run

from voxviz_helper import mesh_cubes

def voxplot(voxels, voxcolor='#999', bgcolor='#444'):
    # t = convolve(voxels, np.ones((5, 5, 5)))
    # t = zoom(t, .25, order=0)
    # return go.Figure(mesh_cubes(t), go.Layout(
    return go.Figure(mesh_cubes(voxels, voxcolor), go.Layout(
        scene=dict(
            aspectmode='cube',
            xaxis_visible=False,
            yaxis_visible=False,
            zaxis_visible=False,
            **{
                ax: dict(
                    type='linear',
                    showgrid=False,
                    showbackground=False,
                    nticks=max(voxels.shape),
                    range=(0, max(voxels.shape)-1),
                )
                for ax in ('xaxis', 'yaxis', 'zaxis')
            }
        ),
        paper_bgcolor=bgcolor,
    ))

def viewlas(lasfile):
    run(f'displaz "{str(lasfile).replace(".npy", ".las")}" &', shell=True)
