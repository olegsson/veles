import warnings
warnings.filterwarnings('ignore')

import utils
tf = utils.tf_init(nogpu=True)

from pathlib import Path
import numpy as np
import xgboost as xgb
from sklearn import metrics
import sklearn.model_selection as modsel
from sklearn import linear_model
import matplotlib.pyplot as plt
plt.style.use('ggplot')
import pandas as pd
import json

import plotutil as pu

#%%

x_vox, y, files = utils.load_data()

ae = tf.keras.models.load_model('models/ae_last.h5')

encoder = tf.keras.Model(ae.layers[0].input, ae.layers[4].output)

x = encoder.predict(x_vox)

# bst = xgb.Booster()
# bst.load_model('models/bst_ev.model')
# bst.save_model('models/bst_ev_compat.model') # compatibility with newer xgboost
# bst.load_model('models/bst_ev_cv.model')
# bst.save_model('models/bst_ev_cv_compat.model') # compatibility with newer xgboost
classifier = xgb.XGBClassifier(verbosity=0)
# classifier.load_model('models/bst_ev_compat.model')
classifier.load_model('models/bst_ev_cv_compat.model')

kfold = modsel.StratifiedKFold(n_splits=3)

splits = [*kfold.split(x, y)]

#%%
fig, ax = plt.subplots(2, 2, figsize=(12, 12))

# i_split = 0
# split_train, split_test = splits[i_split]
results_gbt = {
    'precision': [],
    'recall': [],
    'f1': [],
    'roc_auc': [],
    'pr_auc': [],
}
results_logreg = {
    'precision': [],
    'recall': [],
    'f1': [],
    'roc_auc': [],
    'pr_auc': [],
}
baseline = linear_model.LogisticRegression()

false_pos = []
false_neg = []

for i_split, (split_train, split_test) in enumerate(splits):
    x_train, y_train = x[split_train], y[split_train]
    x_test, y_test = x[split_test], y[split_test]
    classifier.fit(x_train, y_train)
    p = classifier.predict(x_test)
    pprob = classifier.predict_proba(x_test)[:,1]

    fp_idx = p & ~y_test
    fn_idx = y_test & ~p
    false_pos.append(split_test[fp_idx].tolist())
    false_neg.append(split_test[fn_idx].tolist())

    precision, recall, __ = metrics.precision_recall_curve(y_test, pprob)
    pr_auc = metrics.auc(recall, precision)
    fpr, tpr, __ = metrics.roc_curve(y_test, pprob)
    roc_auc = metrics.roc_auc_score(y_test, pprob)

    results_gbt['precision'].append(metrics.precision_score(y_test, p))
    results_gbt['recall'].append(metrics.recall_score(y_test, p))
    results_gbt['f1'].append(metrics.f1_score(y_test, p))
    results_gbt['roc_auc'].append(roc_auc)
    results_gbt['pr_auc'].append(pr_auc)

    ax[0][0].plot(fpr, tpr, label=f'SPLIT {i_split+1}: AUC={roc_auc.round(3)}')
    ax[0][1].plot(recall, precision, label=f'SPLIT {i_split+1}: AUC={pr_auc.round(3)}')

    baseline.fit(x_train, y_train)
    p = baseline.predict(x_test)
    pprob = baseline.predict_proba(x_test)[:,1]

    precision, recall, __ = metrics.precision_recall_curve(y_test, pprob)
    pr_auc = metrics.auc(recall, precision)
    fpr, tpr, __ = metrics.roc_curve(y_test, pprob)
    roc_auc = metrics.roc_auc_score(y_test, pprob)

    results_logreg['precision'].append(metrics.precision_score(y_test, p))
    results_logreg['recall'].append(metrics.recall_score(y_test, p))
    results_logreg['f1'].append(metrics.f1_score(y_test, p))
    results_logreg['roc_auc'].append(roc_auc)
    results_logreg['pr_auc'].append(pr_auc)

    ax[1][0].plot(fpr, tpr, label=f'SPLIT {i_split+1}: AUC={roc_auc.round(3)}')
    ax[1][1].plot(recall, precision, label=f'SPLIT {i_split+1}: AUC={pr_auc.round(3)}')

    ax[0][0].set_title('GBT ROC curve')
    ax[0][0].legend(frameon=False)
    ax[0][1].set_title('GBT precision / recall curve')
    ax[0][1].legend(frameon=False)
    ax[0][0].set_xlabel('false positive rate')
    ax[0][0].set_ylabel('true positive rate')
    ax[0][1].set_xlabel('recall')
    ax[0][1].set_ylabel('precision')

    ax[1][0].set_title('Logistic regression ROC curve')
    ax[1][0].legend(frameon=False)
    ax[1][1].set_title('Logistic regression precision / recall curve')
    ax[1][1].legend(frameon=False)
    ax[1][0].set_xlabel('false positive rate')
    ax[1][0].set_ylabel('true positive rate')
    ax[1][1].set_xlabel('recall')
    ax[1][1].set_ylabel('precision')

plt.savefig('results/roc_pr_curves.png', bbox_inches='tight', dpi=150)

for k, v in results_gbt.items():
    results_gbt[k].append(np.mean(v))

for k, v in results_logreg.items():
    results_logreg[k].append(np.mean(v))

classifier.fit(x, y)
p = classifier.predict(x)
pprob = classifier.predict_proba(x)[:,1]

fp_idx = p & ~y
fn_idx = y & ~p
false_pos.append(np.where(fp_idx)[0].tolist())
false_neg.append(np.where(fn_idx)[0].tolist())

with open('results/results_false.json', 'w') as f:
    json.dump({
        'false_positive': false_pos,
        'false_negative': false_neg,
    }, f, indent=4)

precision, recall, __ = metrics.precision_recall_curve(y, pprob)
pr_auc = metrics.auc(recall, precision)
fpr, tpr, __ = metrics.roc_curve(y, pprob)
roc_auc = metrics.roc_auc_score(y, pprob)

results_gbt['precision'].append(metrics.precision_score(y, p))
results_gbt['recall'].append(metrics.recall_score(y, p))
results_gbt['f1'].append(metrics.f1_score(y, p))
results_gbt['roc_auc'].append(roc_auc)
results_gbt['pr_auc'].append(pr_auc)

results_gbt = pd.DataFrame(results_gbt)
results_gbt.index = [
    *(f'val_{i+1}' for i in range(len(splits))),
    'val_mean',
    'all',
]
results_gbt.to_csv('results/results_gbt.csv')

baseline.fit(x, y)
p = baseline.predict(x)
pprob = baseline.predict_proba(x)[:,1]

precision, recall, __ = metrics.precision_recall_curve(y, pprob)
pr_auc = metrics.auc(recall, precision)
fpr, tpr, __ = metrics.roc_curve(y, pprob)
roc_auc = metrics.roc_auc_score(y, pprob)

results_logreg['precision'].append(metrics.precision_score(y, p))
results_logreg['recall'].append(metrics.recall_score(y, p))
results_logreg['f1'].append(metrics.f1_score(y, p))
results_logreg['roc_auc'].append(roc_auc)
results_logreg['pr_auc'].append(pr_auc)

results_logreg = pd.DataFrame(results_logreg)
results_logreg.index = [
    *(f'val_{i+1}' for i in range(len(splits))),
    'val_mean',
    'all',
]
results_logreg.to_csv('results/results_baseline.csv')
