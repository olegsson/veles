import warnings
warnings.filterwarnings('ignore')

import utils
tf = utils.tf_init(nogpu=True)

import open3d as o3d
from pyntcloud import PyntCloud
import numpy as np
from pathlib import Path
import pandas as pd
import os
import json
from subprocess import run
from random import choice

import plotutil as pu

#%%

x_vox, y, files = utils.load_data()

ae = tf.keras.models.load_model('models/ae_last.h5')

#%%

def plot_sample(i, parent_dir):
    if not Path(parent_dir).exists():
        os.makedirs(parent_dir)
    x_ae = ae.predict(x_vox[i:i+1]) >= .5
    pu.pcdplot(files[i], background='#fff', filename=f'{parent_dir}/{i}_0_pcd.png', block=True)
    pu.voxplot(x_vox[i], background='#fff', filename=f'{parent_dir}/{i}_1_vox.png', block=True)
    pu.voxplot(x_ae, background='#fff', filename=f'{parent_dir}/{i}_2_vox_ae.png', block=True)

run('rm -r tmp_voxplots/random_samples', shell=True)

for __ in range(6):
    i = choice(range(y.size))
    plot_sample(i, f'tmp_voxplots/random_samples')

#%%

run(
    'montage tmp_voxplots/random_samples/*_pcd.png ' \
    'tmp_voxplots/random_samples/*_vox.png ' \
    '-geometry +0+0 -resize 500x500^ -gravity center -crop 500x500+0+0 -tile 3x4 ' \
    'results/samples_pcd+vox.png',
    shell=True,
)

run(
    'montage tmp_voxplots/random_samples/*_vox.png ' \
    'tmp_voxplots/random_samples/*_vox_ae.png ' \
    '-geometry +0+0 -resize 500x500^ -gravity center -crop 500x500+0+0 -tile 3x4 ' \
    'results/samples_vox+ae.png',
    shell=True,
)
