class: center, middle

__SVEUČILIŠTE U ZAGREBU__

__GEODETSKI FAKULTET__

Zavod za geomatiku

### Detekcija speleoloških objekata upotrebom strojnog učenja na vokseliziranom LiDAR oblaku točaka

__Obrana teme diplomskog rada__

Luka Antonić

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

Zagreb, 2020.

---

# Uvod

- LiDAR tehnologijom moguće je prikupiti velike količine visokokvalitetnih podataka o geometriji Zemljine površine.

- Kao alternativa fizičkom pretraživanju terena (skupom i dugotrajnom), LiDAR snimke su korištene i u svrhu pronalaska speleoloških objekata.

- Radi izbjegavanja ručnog pregledavanja oblaka točaka vrlo velikih područja, kao indikator postojanja anomalija u reljefu korištene su karte _lokalnog reljefa_ (Hesse, 2010), međutim dobivanje pouzdane informacije o prisutnosti speleoloških objekata i dalje zahtijeva veću količinu vremena provedenog u pregledavanju izdvojenih anomalija.

---

# Uvod

- Tijekom 2019. obradom LiDAR oblaka točaka NP Plitvička jezera, odnosno izdvajanjem negativnih anomalija u lokalnom reljefu i pregledavanjem izdvojenih isječaka oblaka točaka izrađen je katastar potencijalnih speleoloških objekata na ovom području (Grozić, Antonić, 2019).

- Od približno 1600 izdvojenih negativnih anomalija u reljefu, njih 347 je klasificirano kao potencijalni speleološki objekt.

- Ovaj skup podataka će u radu biti korišten za treniranje klasifikacijskih modela algoritmima strojnog nadziranog učenja, odnosno za uspostavu automatiziranog sustava detekcije (potencijalnih) speleoloških objekata.

---

# Uvod

<!-- ![:scale 50%](./pointcloud_ground.png) -->

![:scale 40%](./jama_da.png) .large[➡ DA]

![:scale 40%](./jama_ne.png) .large[➡ NE]

---

# Metode

- Radi lakše manipulacije podacima, odnosno smanjenja količine podataka (korišteni oblak točaka je gustoće približno 8 točaka po m<sup>2</sup>, sa trodimenzionalnim koordinatama) strojno učenje će se izvoditi na vokseliziranim isječcima oblaka točaka, odnosno trodimenzionalnim _boolean_ rasterima (tenzorima) prostorne rezolucije 1x1x1 m, gdje je vrijednost voksela 1 ukoliko sadrži barem jednu, a 0 ukoliko ne sadrži niti jednu točku.

![:scale 43.5%](./jama_pcd.png) .larger[➡]
![:scale 43.5%](./jama_vox.png)

---

# Metode

![:scale 40%](./jama_vox.png) .large[➡ DA]

![:scale 40%](./jama_ne_vox.png) .large[➡ NE]

---

# Metode

- Za detekciju značajki u rasterskim podacima (pa tako i u 3D rasteru) među algoritmima strojnog učenja izvrsne rezultate su pokazale konvolucijske neuronske mreže, koje se danas redovito koriste u svrhu rasterske klasifikacije.

- Sposobnost generalizacije neuronskih mreža vrlo je ovisna o veličini i sastavu skupa podataka korištenog za trening, te skup koji sadrži 347 pozitivnih uzoraka (speleoloških objekata) i proizvoljnu količinu negativnih nije podoban za direktnu klasifikaciju ovim metodama.

- Autoenkoderi su tip neuronske mreže kod kojih je ulaz jednak izlazu, dok se unutarnjim slojevima postiže najprije kompresija (i smanjenje dimenzionalnosti) ulaznog podatka, a potom rekonstrukcija ulaznog podatka iz komprimiranog oblika

- _Gradient boosting_ algoritmi nadziranog učenja pokazali su visoku otpornost na nedostatke u ulaznom setu podataka te se redovito koriste u svrhu klasifikacije podataka koji sadrže proizvoljan broj skalarnih varijabli (1D vektor ulaznog podatka)

---

# Metode

- Budući da za treniranje autoenkodera nije bitna klasifikacijska struktura ulaznih podataka, već samo njegova sposobnost kompresije reljefnih oblika uz što manji gubitak kvalitete podataka, ovakav tip neuronske mreže koristit će se u svrhu izdvajanja reljefnih značajki u 1D oblik.

![:scale 38%](./jama_vox.png) .large[➡]
`\(
    \begin{bmatrix}
        x_1 \\
        x_2 \\
        \vdots \\
        x_n \\
    \end{bmatrix}
\)`
.large[➡]
![:scale 38%](./jama_vox_encoded.png)

---

# Metode

.inline_codeblock[```python
x = layers.Input(x_train.shape[1:])
e = layers.Conv3D(32, 7, 4, activation='relu', padding='same')(x)
e = layers.Conv3D(32, 5, 2, activation='relu', padding='same')(e)
e = layers.Flatten()(e)
```]
enkoder .large[⬇]

.inline_codeblock[```python
e = layers.Dense(512, activation='relu')(e)
```]
komprimirani oblik .large[⬇]

.inline_codeblock[```python
d = layers.Reshape((8, 8, 8, 1))(e)
d = layers.Conv3DTranspose(32, 5, 2, padding='same', activation='relu')(d)
d = layers.Conv3DTranspose(32, 7, 4, padding='same', activation='relu')(d)
d = layers.MaxPool3D((2, 2, 2), strides=2)(d)
d = layers.Conv3D(1, 1, 1, activation='sigmoid', padding='same')(d)
```]
dekoder

---

# Metode

- Dobiveni 1D oblik podatka o reljefu, odnosno vektor reljefnih značajki, bit će korišten kao ulazni podatak za treniranje _gradient boosting trees_ klasifikacijskog modela

`\(
    \begin{bmatrix}
        x_1 \\
        x_2 \\
        \vdots \\
        x_n \\
    \end{bmatrix}
\)`
.large[➡ DA / NE]

.inline_codeblock[```python
bst = xgb.train(
    {
        'max_depth':2,
        'eta':1,
        'objective':'binary:logistic',
    },
    dtrain,
    num_boost_round=10,
)
```]

---

# Metode

![:scale 25%](./jama_pcd.png)
.large[➡]
![:scale 25%](./jama_vox.png)
.large[➡]
`\(
    \begin{bmatrix}
        x_1 \\
        x_2 \\
        \vdots \\
        x_n \\
    \end{bmatrix}
\)`
.large[➡ DA]

.inline_codeblock[```python
vox = voxelize(x)
e = encoder.predict(vox)
p = bst.predict(xgb.DMatrix(e)) >= .5
```]

---

# Preliminarni rezultati

- Tijekom zimskog semestra ak. god. 2019/20, u sklopu izbornog kolegija Programiranje u geoinformacijskim sustavima, provedena je procjena svrhovitosti ove metode.

- Preliminarni model je pokazao vrlo dobre rezultate: ≈90% klasifikacijsku točnost (_f1 score_) na validacijskom, odnosno ≈97% na ukupnom skupu podataka.

<br>

||
|-|-|
| **precision:** | 0.976 |
| **recall:**    | 0.955 |
| **f1 score:**  | 0.966 |

<br>

| | predicted_false | predicted_true |
|-|-|-|
| **false** | 964 |   8 |
| **true**  |  15 | 322 |


---

## Literatura

Chen, T., Guestrin, C., 2016: _XGBoost: A Scalable Tree Boosting System_, Proceedings of the 22nd ACM SIGKDD International Conference on Knowledge Discovery and Data Mining, ACM, 785-794, San Francisco, California, USA

Goodfellow, I., Benigio Y., Courville A., 2016: _Deep Learning_, MIT Press

Grozić, D., Kukuljan, L., Bočić, N., 2017: _Using LiDAR ALS data as a supplement to existing cave registry data: an example from NW Gorski kotar, Croatia_, 13th International Conference on Geoheritage, Geoinformation and Cartography, Selce, HR

Grozić, D., Antonić, L., 2019: _Katastar potencijalnih speleoloških objekata NP Plitvička jezera_, Geonatura, Zagreb, HR

Hesse, R., 2010: _LiDAR-derived Local Relief Models - a newtool for archaeological prospection_, Archaeological Prospection, 17, 67-72

Moyes, H., Montgomery, S., 2018: _Locating Cave Entrances Using Lidar-Derived Local Relief Modeling_, Geosciences, 9, 98
