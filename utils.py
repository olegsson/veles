from pathlib import Path
import numpy as np

def load_data():
    y, files = zip(*[(1, fp) for fp in Path('/data/work/lidar_plitvice/plitvice_voxels').glob('*.npy')] \
        + [(0, fp) for fp in Path('/data/work/lidar_plitvice/plitvice_voxels_negative').glob('*.npy')])
    x = np.stack([
        np.load(fp)
        for fp in files
    ]).astype(np.float16)
    return x.reshape(*x.shape, 1), np.array(y, dtype=bool), files

def tf_init(gpu_memlimit=2048, nogpu=False):
    if nogpu:
        import os
        os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    import tensorflow as tf
    if not nogpu:
        gpu = tf.config.experimental.list_physical_devices('GPU')[0]
        tf.config.experimental.set_visible_devices(gpu, 'GPU')
        tf.config.experimental.set_virtual_device_configuration(
            gpu,
            [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=gpu_memlimit)],
        )
    return tf

if __name__ == '__main__':
    print(load_data())
