class: center, middle

__SVEUČILIŠTE U ZAGREBU__

__GEODETSKI FAKULTET__

Zavod za geomatiku

### Detekcija speleoloških objekata upotrebom strojnog učenja na vokseliziranom LiDAR oblaku točaka

__Prezentacija rezultata istraživanja u sklopu diplomskog rada__

Luka Antonić

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

Zagreb, 2020.

---

# Uvod

- LiDAR tehnologijom moguće je prikupiti velike količine visokokvalitetnih podataka o geometriji Zemljine površine.

- Kao alternativa fizičkom pretraživanju terena (skupom i dugotrajnom), LiDAR snimke su korištene i u svrhu pronalaska speleoloških objekata.

- Radi izbjegavanja ručnog pregledavanja oblaka točaka vrlo velikih područja, kao indikator postojanja anomalija u reljefu korištene su karte _lokalnog reljefa_ (Hesse, 2010), međutim dobivanje pouzdane informacije o prisutnosti speleoloških objekata i dalje zahtijeva veću količinu vremena provedenog u pregledavanju izdvojenih anomalija.

---

# Uvod

- Tijekom 2019. obradom LiDAR oblaka točaka NP Plitvička jezera, odnosno izdvajanjem negativnih anomalija u lokalnom reljefu i pregledavanjem izdvojenih isječaka oblaka točaka izrađen je katastar potencijalnih speleoloških objekata na ovom području (Grozić, Antonić, 2019).

- Od približno 1600 izdvojenih negativnih anomalija u reljefu, njih 347 je klasificirano kao potencijalni speleološki objekt.

- Trenutno je u tijeku terensko pretraživanje lokacija izdvojenih u katastru potencijalnih speleoloških objekata, te se pretpostavlja da će biti dovršeno do jeseni 2020.

---

# Priprema podataka

- U svrhu pronalaska negativnih anomalija u reljefu tijekom izrade katastra potencijalnih speleoloških objekata, iz oblaka točaka je izrađena karta _lokalnog reljefa_ sljedećim koracima:
 - reklasifikacija tla u oblaku točaka
 - izrada digitalnog modela reljefa prostorne rezolucije 20 cm
 - zaglađivanje DMR-a računanjem fokalnog srednjaka visine u prozoru dimenzije 11 m
 - izrada karte anomalija oduzimanjem zaglađenog DMR-a od prvobitnog
 - izrada slojnica iz karte anomalija i interpolacija DMR-a iz okolnih vrijednosti unutar slojnica vrijednosti nula
 - oduzimanje interpoliranog DMR-a od prvobitnog te izdvajanje negativnih anomalija

![:scale 100%](./karta_anomalija.png)

<!-- ![:scale 40%](./jama_da.png) .large[➡ DA] -->

<!-- ![:scale 40%](./jama_ne.png) .large[➡ NE] -->

---

# Priprema podataka

- Isječci oblaka točaka (bufferi radijusa 30 m oko negativnih anomalija) potom su manualno pregledavani te je ekspertnom procjenom utvrđeno radi li se o potencijalnom speleološkom objektu
- Izrađeni su novi isječci (bufferi radijusa 10 m) oko lokacija potencijalnih objekata te 1000 isječaka oko nasumično odabranih točaka koje se nalaze minimalno 20 m od najbližeg potencijalnog objekta
- Isječci su potom vokselizirani, odnosno iz njih su dobiveni 3D binarni tenzori dimenzija 32x32x32 i prostorne rezolucije 1 m, pri čemu je vrijednost voksela jednaka jedan ukoliko se u njemu nalazi barem jedna točka, a u protivnome jednaka nuli; pri tome je izostavljeno 10 isječaka potencijalnih objekata i 28 isječaka nasumičnih lokacija, zbog problema s geometrijom pri vokselizaciji te je dobiveno ukupno 1309 uzoraka

---

# Metode klasifikacije

- Uspostavljeni klasifikacijski sustav sastoji se od autoenkodera, odnosno njegovog enkoderskog dijela kojime se dobiva vektor značajki reljefa, te _gradient boosting trees_ klasifikatora
- Različite topologije atoenkodera trenirane su na 80% uzoraka te validirane na ostalih 20% kroz 100 epoha, koristeći _tensorflow_ paket (kroz _Keras_ sučelje)
- Kao kriterij za odabir topologije, osim same _loss_ funkcije korištene tijekom treninga (_binary crossentropy_), uzeta je i vizualno procijenjena postojanost strukture tenzora nakon kompresije i rekonstrukcije
- Odabran je autoenkoder kojim se iz 3D tenzora dobiva vektor od 512 značajki reljefa

---

# Metode klasifikacije

- Topologija odabranog autoenkodera i odvojen enkoderski dio kojime se dobiva vektor značajki:

![:scale 38%](./ae.png)
 <!-- .larger[➡] -->
![:scale 38%](./encoder.png)

---

# Metode klasifikacije

- Klasifikator, odnosno _gradient boosted trees_ model implementiran _XGBoost_ paketom, parametriziran je korištenjem genetskog algoritma: evaluirana je populacija od 100 klasifikatora kroz 10 generacija, pri čemu je _fitness_ funkcija definirana kao _f1 score_ klasifikatora na validacijskom uzorku

- Prvo (od 10) _drvo_ u strukturi dobivenog klasifikatora:

![:scale 100%](./bst0.png)

<!-- .large[➡ DA] -->

<!-- ![:scale 40%](./jama_ne_vox.png) .large[➡ NE] -->

---

# Rezultati

- Dobiveni klasifikacijski sustav pokazao je poboljšanje u odnosu na preliminarne rezultate, odnosno ≈92% klasifikacijsku točnost (_f1 score_) na validacijskom uzorku te ≈99% na ukupnom uzorku

- Rezultati klasifikacije na validacijskom uzorku:
<br>

||
|-|-|
| **precision:** | 0.918 |
| **recall:**    | 0.918 |
| **f1 score:**  | 0.918 |

<br>

| | predicted_false | predicted_true |
|-|-|-|
| **false** | 196 |   5 |
| **true**  |   5 |  56 |

---

# Rezultati

- Rezultati klasifikacije na ukupnom uzorku:
<br>

||
|-|-|
| **precision:** | 0.985 |
| **recall:**    | 0.985 |
| **f1 score:**  | 0.985 |

<br>

| | predicted_false | predicted_true |
|-|-|-|
| **false** | 967 |   5 |
| **true**  |   5 | 332 |

---

## Literatura

Chen, T., Guestrin, C., 2016: _XGBoost: A Scalable Tree Boosting System_, Proceedings of the 22nd ACM SIGKDD International Conference on Knowledge Discovery and Data Mining, ACM, 785-794, San Francisco, California, USA

Goodfellow, I., Benigio Y., Courville A., 2016: _Deep Learning_, MIT Press

Grozić, D., Kukuljan, L., Bočić, N., 2017: _Using LiDAR ALS data as a supplement to existing cave registry data: an example from NW Gorski kotar, Croatia_, 13th International Conference on Geoheritage, Geoinformation and Cartography, Selce, HR

Grozić, D., Antonić, L., 2019: _Katastar potencijalnih speleoloških objekata NP Plitvička jezera_, Geonatura, Zagreb, HR

Hesse, R., 2010: _LiDAR-derived Local Relief Models - a new tool for archaeological prospection_, Archaeological Prospection, 17, 67-72

Moyes, H., Montgomery, S., 2018: _Locating Cave Entrances Using Lidar-Derived Local Relief Modeling_, Geosciences, 9, 98
