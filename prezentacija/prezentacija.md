class: center, middle

# Detekcija speleoloških objekata iz LiDAR snimaka

.imgfs[![](./pointcloud.png)]

---

class: center, middle

![:scale 70%](./pointcloud_ground.png)

![:scale 40%](./jama_da.png) .large[➡ DA]

![:scale 40%](./jama_ne.png) .large[➡ NE]

---

class: center, middle

![:scale 40%](./jama_pcd.png) .larger[➡]
![:scale 40%](./jama_vox.png)

![:scale 35%](./jama_vox.png) .large[➡ DA]

![:scale 35%](./jama_ne_vox.png) .large[➡ NE]

---

class: center, middle

![:scale 25%](./jama_vox.png) .large[➡]
`\(
    \begin{bmatrix}
        x_1 \\
        x_2 \\
        \vdots \\
        x_n \\
    \end{bmatrix}
\)`
.large[➡]
![:scale 25%](./jama_vox_encoded.png)

.inline_codeblock[```python
x = layers.Input(x_train.shape[1:])
e = layers.Conv3D(32, 7, 4, activation='relu', padding='same')(x)
e = layers.Conv3D(32, 5, 2, activation='relu', padding='same')(e)
e = layers.Flatten()(e)
```]
.large[⬇]

.inline_codeblock[```python
e = layers.Dense(512, activation='relu')(e)
```]
.large[⬇]

.inline_codeblock[```python
d = layers.Reshape((8, 8, 8, 1))(e)
d = layers.Conv3DTranspose(32, 5, 2, padding='same', activation='relu')(d)
d = layers.Conv3DTranspose(32, 7, 4, padding='same', activation='relu')(d)
d = layers.MaxPool3D((2, 2, 2), strides=2)(d)
d = layers.Conv3D(1, 1, 1, activation='sigmoid', padding='same')(d)
```]

---

class: center, middle

`\(
    \begin{bmatrix}
        x_1 \\
        x_2 \\
        \vdots \\
        x_n \\
    \end{bmatrix}
\)`
.large[➡ DA / NE]

.inline_codeblock[```python
bst = xgb.train(
    {
        'max_depth':2,
        'eta':1,
        'objective':'binary:logistic',
    },
    dtrain,
    num_boost_round=10,
)
```]

---

class: center, middle

![:scale 25%](./jama_pcd.png)
.large[➡]
![:scale 25%](./jama_vox.png)
.large[➡]
`\(
    \begin{bmatrix}
        x_1 \\
        x_2 \\
        \vdots \\
        x_n \\
    \end{bmatrix}
\)`
.large[➡ DA]

.inline_codeblock[```python
vox = voxelize(x)
e = encoder.predict(vox)
p = bst.predict(xgb.DMatrix(e)) >= .5
```]

<br>

||
|-|-|
| **precision:** | 0.976 |
| **recall:**    | 0.955 |
| **f1 score:**  | 0.966 |

<br>


| | predicted_false | predicted_true |
|-|-|-|
| **false** | 964 |   8 |
| **true**  |  15 | 322 |
