import warnings
warnings.filterwarnings('ignore')

import utils
tf = utils.tf_init(nogpu=True)

from pathlib import Path
from random import choice
import numpy as np
import xgboost as xgb
from sklearn import metrics
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import sklearn.model_selection as modsel
from sklearn import linear_model
import matplotlib.pyplot as plt
plt.style.use('ggplot')
import pandas as pd
import json

import plotutil as pu

#%%

x_vox, y, files = utils.load_data()

ae = tf.keras.models.load_model('models/ae_last.h5')

encoder = tf.keras.Model(ae.layers[0].input, ae.layers[4].output)

x = encoder.predict(x_vox)
scaler = StandardScaler()
x_scaled = scaler.fit_transform(x)

classifier = xgb.XGBClassifier(verbosity=0)
classifier.load_model('models/bst_ev_cv_compat.model')

kfold = modsel.StratifiedKFold(n_splits=3)

baseline = linear_model.LogisticRegression()

n_scale = [round(2**(p/2)) for p in range(1, 19)]
var_scale = [*np.arange(.5, .96, .05).round(2), *(1 - np.logspace(-2, -5, 4))]

#%%

def compute_pca(n_components):
    pca = PCA(n_components=n_components, svd_solver='auto', whiten=True)
    return pca.fit_transform(x_scaled)

n_comps = [
    compute_pca(var).shape[1]
    for var in var_scale
]

#%%

fig, ax = plt.subplots(figsize=(12, 4))

ax.plot(n_comps)
ax.set_xticks(range(len(n_comps)))
ax.set_xticklabels(var_scale, rotation=45)
ax.set_xlabel('variability')
ax.set_ylabel('n_comonents')
ax.set_title('Number of principal components per feature space variability ratio')

plt.savefig('results/pca_n_var.png', bbox_inches='tight', dpi=150)

pd.DataFrame({
    'variability': var_scale,
    'n_components': n_comps,
}).to_csv('results/pca_n_var.csv')

#%%

scoring = [
    'precision',
    'recall',
    'f1',
    'roc_auc',
]

def compute_pca_scores(model):
    def wrapped(n_components):
        pc = compute_pca(n_components)
        scores = modsel.cross_validate(
            model, pc, y,
            cv=kfold,
            scoring=[
                'precision',
                'recall',
                'f1',
                'roc_auc',
            ]
        )
        scores = pd.DataFrame(scores)
        scores.columns = [col.replace('test_', '') for col in scores.columns]
        scores = scores[scoring]
        return scores.mean()
    return wrapped

#%%

xgb_pca_by_n = pd.DataFrame(map(compute_pca_scores(classifier), n_scale))
xgb_pca_by_n['n_components'] = n_scale

xgb_pca_by_var = pd.DataFrame(map(compute_pca_scores(classifier), var_scale))
xgb_pca_by_var['variability'] = var_scale

logreg_pca_by_n = pd.DataFrame(map(compute_pca_scores(baseline), n_scale))
logreg_pca_by_n['n_components'] = n_scale

logreg_pca_by_var = pd.DataFrame(map(compute_pca_scores(baseline), var_scale))
logreg_pca_by_var['variability'] = var_scale

xgb_pca_by_n.to_csv('results/pca_gbt_n.csv', index=False)
xgb_pca_by_var.to_csv('results/pca_gbt_var.csv', index=False)
logreg_pca_by_n.to_csv('results/pca_baseline_n.csv', index=False)
logreg_pca_by_var.to_csv('results/pca_baseline_var.csv', index=False)

#%%

fig, ax = plt.subplots(4, 1, figsize=(12, 16))
fig.tight_layout(h_pad=5)

for i, df in enumerate((
    xgb_pca_by_n,
    xgb_pca_by_var,
    logreg_pca_by_n,
    logreg_pca_by_var,
)):
    ax_ = ax[i]
    x_ticks = [*range(df.shape[0])]
    for score in scoring:
        ax_.plot(x_ticks, df[score], label=score)
    scale_name = df.columns[-1]
    ax_.set_xlabel(scale_name)
    ax_.set_xticks(x_ticks)
    ax_.set_xticklabels(df[scale_name])
    ax_.set_ylabel('score')
    ax_.legend(frameon=False)

ax[0].set_title('GBT scores with PCA by N components')
ax[1].set_title('GBT scores with PCA by variability ratio')
ax[2].set_title('Logistic regression scores with PCA by N components')
ax[3].set_title('Logistic regression scores with PCA by variability ratio')

plt.savefig('results/pca_scores.png', bbox_inches='tight', dpi=150)
