import pdal
from pathlib import Path
import geopandas as gp
import json

def sample_extractor(lasdir, outdir):
    lasdir = Path(lasdir)
    outdir = Path(outdir)
    def wrapped(record, buffer_size=10.):
        try:
            lasfile = next(lasdir.glob(f'{record.origin}_ground.las'))
            print('cropping', lasfile)
        except StopIteration:
            print(record.origin, 'not found')
            return
        outfile = outdir.joinpath(f'{record.id}_{record.origin}.las')
        pdal.Pipeline(json.dumps({'pipeline': [
            {
                'type': 'readers.las',
                'override_srs': 'epsg:3765',
                'filename': lasfile.as_posix(),
            },
            {
                'type': 'filters.crop',
                'polygon': record.geometry.buffer(buffer_size).to_wkt(),
            },
            {
                'type': 'writers.las',
                'minor_version': '2',
                'dataformat_id': '3',
                'filename': outfile.as_posix(),
            },
        ]})).execute()
    return wrapped

sampler = sample_extractor(
    '/data/Sismisi/LiDAR/plitvice_obrada/',
    '/home/luka/plitvice_voxels/',
    # '/data/work/lidar_plitvice/missing/',
    # '/data/work/lidar_plitvice/plitvice_voxels/',
)

poi = gp.read_file('pointcloud_sredjeno.geojson')
poi.apply(sampler, axis=1)
