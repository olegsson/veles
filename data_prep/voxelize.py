from pathlib import Path
import numpy as np
from pyntcloud import PyntCloud
from multiprocessing import Pool

def voxfile(lasfile, outdir='./tmp'):
    return Path(outdir).joinpath(lasfile.name.replace('.las', '.npy'))

# def voxelize(args, voxelsize=.25):
def voxelize(args, voxelsize=1.):
# def voxelize(args, voxelsize=2.):
    lasfile, outfile = args
    try:
        pc = PyntCloud.from_file(lasfile.as_posix())
        voxid = pc.add_structure('voxelgrid', size_x=voxelsize, size_y=voxelsize, size_z=voxelsize)
        # out = np.zeros((128, 128, 128), dtype=bool)
        out = np.zeros((32, 32, 32), dtype=bool)
        # out = np.zeros((16, 16, 16), dtype=bool)
        voxels = pc.structures[voxid].get_feature_vector(mode='binary').astype(bool)
        out[:voxels.shape[0], :voxels.shape[1], :voxels.shape[2]] = voxels
        # return out
        np.save(outfile, out)
        print('done with', outfile)
    except Exception as e:
        print('error with', lasfile, '\n', e)

#%%

def voxelize_dir(voxdir):
    [*lasfiles] = Path(voxdir).glob('*.las')
    outfiles = [voxfile(lf, voxdir) for lf in lasfiles]
    with Pool(8) as p:
        p.map(voxelize, zip(lasfiles, outfiles))

voxelize_dir('/data/work/lidar_plitvice/plitvice_voxels')
voxelize_dir('/data/work/lidar_plitvice/plitvice_voxels_negative')

# sets = zip(lasfiles, outfiles)
# voxelize(*next(sets)).shape
# # np.save('tmp/voxels.npy', voxels)
# %timeit -n1 __ = np.load(outfiles[0])

#%%
# import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D
#
# fig = plt.figure()
# ax = fig.gca(projection='3d')
# ax.voxels(np.load(outfiles[1]))
# fig.show()
