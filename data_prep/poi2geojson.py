import pandas as pd
import geopandas as gp
import shapely.geometry as sg
import re

poi = pd.read_csv('pointcloud_sredjeno.txt', sep='\t')
poi['geometry'] = poi.apply(lambda r: sg.Point(r.x, r.y), axis=1)
poi = gp.GeoDataFrame(poi)
poi['id'] = range(poi.index.size)
poi['origin'] = poi.Name.apply(lambda n: re.findall(r'[0-9]+_[0-9]+', n)[0])
poi.to_file('pointcloud_sredjeno.geojson', driver='GeoJSON')
# print('\n'.join(map(lambda o: o+'.las', poi.origin.unique())))
