from random import choice
from pathlib import Path
import numpy as np
from shapely import speedups
if speedups.available:
    speedups.enable()
from shapely.geometry import Point
from pyntcloud import PyntCloud
import geopandas as gp
import pdal
import json
from multiprocessing import Pool

# lasdir = '/data/work/lidar_plitvice/missing'
# outdir = '/data/work/lidar_plitvice/plitvice_voxels_negative'
lasdir = '/data/Sismisi/LiDAR/plitvice_obrada/'
outdir = '/home/luka/plitvice_voxels_negative'
files = sorted(map(str, Path(lasdir).glob('*_ground.las')))

poi = gp.read_file('pointcloud_sredjeno.geojson')
buffer = poi.buffer(30, resolution=4).cascaded_union

def randpoint(b):
    w = b[2] - b[0]
    h = b[3] - b[1]
    return Point(np.random.random() * w + b[0], np.random.random() * h + b[1])

def make_negative(i, buffer_size=10.):
    print('making negative', i)
    f = choice(files)
    pc = PyntCloud.from_file(f)
    x, y = np.stack(pc.xyz[:,:2], axis=1)
    b = x.min(), y.min(), x.max(), y.max()
    while True:
        pt = randpoint(b)
        if not pt.buffer(buffer_size*3, resolution=4).intersects(buffer):
            outfile = Path(outdir).joinpath(f'{i}_{Path(f).name}')
            pdal.Pipeline(json.dumps({'pipeline': [
                {
                    'type': 'readers.las',
                    'override_srs': 'epsg:3765',
                    'filename': f,
                },
                {
                    'type': 'filters.crop',
                    'polygon': pt.buffer(buffer_size).to_wkt(),
                },
                {
                    'type': 'writers.las',
                    'minor_version': '2',
                    'dataformat_id': '3',
                    'filename': outfile.as_posix(),
                },
            ]})).execute()
            return

with Pool(10) as p:
    __ = p.map(make_negative, range(1000))
