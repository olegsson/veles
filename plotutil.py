from subprocess import run
import open3d as o3d
from colour import Color
import numpy as np
from multiprocessing import Process
from pyntcloud import PyntCloud

def draw(
    geoms, background='#444',
    rotate=(200, -500),
    filename=None,
):
    if isinstance(background, str):
        background = Color(background).rgb
    vis = o3d.visualization.Visualizer()
    vis.create_window()
    vis.get_render_option().background_color = background
    vis.get_render_option().line_width = 5
    [vis.add_geometry(g) for g in geoms]
    ctr = vis.get_view_control()
    if rotate is not None:
        ctr.rotate(0, rotate[1])
        ctr.rotate(rotate[0], 0)
    if filename is not None:
        vis.capture_screen_image(filename, do_render=True)
    vis.run()
    vis.destroy_window()

def voxplot(
    voxin, facecolor='#222',
    edgecolor='#444', background='#444',
    block=False,
    filename=None,
):

    voxels = voxin.reshape([
        d for d in voxin.shape
        if d != 1
    ])

    if isinstance(facecolor, str):
        facecolor = Color(facecolor).rgb
    if isinstance(edgecolor, str):
        edgecolor = Color(edgecolor).rgb
    geoms = []
    if facecolor is not None:
        for xyz in zip(*np.where(voxels.astype(np.uint8))):
            box = o3d.geometry.TriangleMesh.create_box(1, 1, 1)
            box.translate(xyz)
            box.compute_vertex_normals()
            box.paint_uniform_color(facecolor)
            geoms.append(box)
    if edgecolor is not None:
        for xyz in zip(*np.where(voxels.astype(np.uint8))):
            lineset = o3d.geometry.LineSet()
            lineset.points = o3d.utility.Vector3dVector([
                [0, 0, 0], [1, 0, 0],
                [0, 1, 0], [1, 1, 0],
                [0, 0, 1], [1, 0, 1],
                [0, 1, 1], [1, 1, 1]
            ])
            lineset.lines = o3d.utility.Vector2iVector([
                [0, 1], [0, 2], [1, 3],
                [2, 3], [4, 5], [4, 6],
                [5, 7], [6, 7], [0, 4],
                [1, 5], [2, 6], [3, 7]
            ])
            lineset.translate(xyz)
            lineset.paint_uniform_color(edgecolor)
            geoms.append(lineset)
    if block:
        draw(geoms, background, filename=filename)
    else:
        t = Process(target=draw, args=(geoms, background), kwargs={'filename':filename})
        t.start()

def pcdplot(
    npyfile, facecolor='#222',
    background='#444',
    block=False,
    filename=None,
):
    if isinstance(facecolor, str):
        facecolor = Color(facecolor).rgb
    pcd = PyntCloud.from_file(str(npyfile).replace('.npy', '.las')).to_instance('open3d')
    xyz = np.asarray(pcd.points)
    xyz -= xyz.min(0)
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(xyz)
    pcd.paint_uniform_color(facecolor)
    if block:
        draw([pcd], background, filename=filename)
    else:
        t = Process(target=draw, args=([pcd], background), kwargs={'filename':filename})
        t.start()

def viewlas(lasfile):
    run(f'displaz "{str(lasfile).replace(".npy", ".las")}" &', shell=True)

if __name__ == '__main__':
    voxplot(np.random.random((5, 5, 5))>.5)
