import random
from itertools import chain
from statistics import mean, median

def _sort(l):
    return sorted(l, key=lambda tup: tup[1])

class _FitnessWrap:
    def __init__(self, fitness):
        self.fitness = fitness

    def __call__(self, individual):
        return individual[0], self.fitness(individual[0])

class Population:
    def __init__(
        self, generator_function, fitness,
        size, fitness_max=True, hof_size=5, pool=None,
        *args, **kwargs
        ):

        self.generate = lambda *dummy: (generator_function(*args, **kwargs), None)
        self.fitness = _FitnessWrap(fitness)
        self.fitness_max = fitness_max
        self.size = size
        self.individuals = [self.generate() for i in range(size)]
        self.hall_of_fame = []
        self.hof_size = hof_size
        self.generation = 0
        self.score = None
        self.pool = pool
        self.generation_evaluated = False

    def _put_in_hof(self, *individuals):
        if not self.hof_size:
            return
        self.hall_of_fame = _sort(
            self.hall_of_fame + [
                (*individual, self.generation)
                for individual in individuals
            ]
        )
        if self.fitness_max:
            self.hall_of_fame = self.hall_of_fame[::-1]
        if self.hof_size > 0:
            self.hall_of_fame = self.hall_of_fame[:self.hof_size]

    def _breed(self, *individuals):
        if len(individuals) < 1:
            return self.generate()
        properties, weights = zip(*individuals)
        properties = zip(*properties)
        if not self.fitness_max:
            t = sum(weights) or 1
            weights = list(map(lambda w: 1 - w/t, weights))
        return [
            random.choices(property, weights)[0]
            for property in properties
        ], None

    def _mutate(self, individual, mutate_probability):
        dummy = self.generate()[0]
        properties = list(individual[0])
        for i, prop in enumerate(individual):
            if random.uniform(0, 1) <= mutate_probability:
                properties[i] = dummy[i]
        return properties, None

    def evaluate(self, pool=None):
        pool = pool or self.pool
        try:
            _map = pool.mappop.hall_of_fame
        except AttributeError:
            _map = map

        self.individuals = _sort(_map(self.fitness, self.individuals))
        if self.fitness_max:
            self.individuals = self.individuals[::-1]
        __, fitnesses = zip(*self.individuals)
        self.score = {
            'generation': self.generation,
            'min': min(fitnesses),
            'max': max(fitnesses),
            'mean': mean(fitnesses),
            'median': median(fitnesses),
        }
        self.generation_evaluated = True
        return self.score

    def evolve(
        self, pool=None, mutate_probability=.2,
        keep_best=.25, keep_losers=.25, make_offspring=.25
        ):

        if not self.generation_evaluated:
            self.evaluate(pool)

        self._put_in_hof(*self.individuals[:self.hof_size])

        n_best = int(self.size * keep_best)
        n_losers = int(self.size * keep_losers)
        n_offspring = int(self.size * make_offspring)
        n_newbies = self.size - n_best - n_losers - n_offspring
        breeders = self.individuals[:n_best] + random.sample(self.individuals[n_best:], n_losers)

        # newborn = map(lambda i: self._breed(*breeders), range(n_newborn))
        # mutated = map(lambda i: self._mutate(i, mutate_probability), breeders)
        #
        # self.individuals = chain(mutated, newborn)

        self.individuals = [
            self._breed(*breeders) for i in range(n_offspring)
        ] + [
            self._mutate(i, mutate_probability) for i in breeders
        ] + [
            self.generate() for i in range(n_newbies)
        ]

        self.generation += 1
        self.generation_evaluated = False
        return self.score

if __name__ == '__main__':
    import math

    random.seed(13)
    def genfunc():
        return [random.randrange(4) for i in range(10)]

    def fitness(i):
        probs = map(lambda e: i.count(e) / len(i), i)
        return sum(map(lambda p: -p * math.log(p), probs))

    pop = Population(genfunc, fitness, 100, fitness_max=False)

    for i in range(100):
        pop.evolve()

    print(pop.score, '\n', pop.hall_of_fame)




#
